const fs = require('fs');

class Sdb {
	constructor() {
		this.filename = 'store.txt';
		this.db = new Map(); // Just in case this is used in a long-lived process, caches values
		try {
			this.db = new Map(JSON.parse(fs.readFileSync(this.filename)));
		} catch (e) {
			// We don't really care too much right now, this is safe to ignore
		}
	}

	/**
	 * Add or update a value in the database.
	 * @param key The name of the key to store the value under.
	 * @param value The value to be stored.
	 */
	set(key, value) {
		this.db.set(key, value);
		try {
			fs.writeFileSync(this.filename, JSON.stringify([...this.db]));
		} catch (e) {
			// We don't really care too much right now, this is safe to ignore
		}
	}

	/**
	 * Get a specific value from the database.
	 * @param key The name of the key the value is stored under.
	 * @returns {any}
	 */
	get(key) {
		let result = this.db.get(key);
		// In case the file was written to by another process, re-read it
		if (typeof result === undefined || result === undefined) {
			try {
				this.db = new Map(JSON.parse(fs.readFileSync(this.filename)));
			} catch (e) {
				// We don't really care too much right now, this is safe to ignore
			}
			result = this.db.get(key);
		}

		return result;
	}

	/**
	 * Return an associative array of the database in insert order.
	 * @returns {[K , V][]}
	 */
	getAll() {
		return Array.from(this.db);
	}

	/**
	 * Remove a specific value from the database.
	 * @param key The name of the key/value pair to remove.
	 */
	remove(key) {
		this.db.delete(key);
		try {
			fs.writeFileSync(this.filename, JSON.stringify([...this.db]));
		} catch (e) {
			// We don't really care too much right now, this is safe to ignore
		}
	}

	/**
	 * Remove all values from the database.
	 */
	removeAll() {
		this.db.clear();
		try {
			fs.unlinkSync(this.filename);
		} catch (e) {
			// We don't really care too much right now, this is safe to ignore
		}
	}
}

module.exports = Sdb;

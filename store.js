#!/usr/bin/env node

const sdb = require('./db');
const db = new sdb();
const Error = require('./errors');
let error = null;

switch (process.argv[2]) {
	/**
	 * Add a key/value pair to the database.
	 * @example ./store add KeyName "Value"
	 */
	case 'add':
		if (!process.argv[3]) {
			error = Error.ERR_MISSING_KEY;
			break;
		}
		db.set(process.argv[3], process.argv[4]);
		break;
	/**
	 * Get a value from the database.
	 * @example ./store get KeyName
	 */
	case 'get':
		if (!process.argv[3]) {
			error = Error.ERR_MISSING_KEY;
			break;
		}
		console.log(db.get(process.argv[3]));
		break;
	/**
	 * Remove a value from the database.
	 * @example ./store remove KeyName
	 */
	case 'remove':
		if (!process.argv[3]) {
			error = Error.ERR_MISSING_KEY;
			break;
		}
		db.remove(process.argv[3]);
		break;
	/**
	 * List all values in the database.
	 * @example ./store list
	 */
	case 'list':
		db.getAll().forEach(([key, value]) => {
			console.log(`${key} => ${value}`);
		});
		break;
	/**
	 * Remove all keys and values from the database.
	 * @example ./store clear
	 */
	case 'clear':
		db.removeAll();
		break;
	default:
		error = Error.ERR_MISSING_CMD;
}

if (error) {
	console.log(error);
}
